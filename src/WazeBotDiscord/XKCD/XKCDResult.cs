﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WazeBotDiscord.XKCD
{
    class XKCDResult
    {
        public string ImageURL { get; set; }

        public string AltText { get; set; }

        public string Title { get; set; }
    }
}
