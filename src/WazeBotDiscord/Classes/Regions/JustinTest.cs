﻿namespace WazeBotDiscord.Classes.Servers
{
    public static class JustinTestServerChannels
    {
        public const ulong StateManagers = 360598522141343747;
        public const ulong LargeAreaManagers = 299571569343070212;
        public const ulong AreaManagers = 360598672276324355;
        /*public const ulong Illinois = 299563350327689228;
        public const ulong Indiana = 299563255934746624;
        public const ulong Michigan = 299567520652853248;
        public const ulong Ohio = 299563222107815936;
        public const ulong Wisconsin = 299563313203904512;*/
    }

    public static class JustinTestServerRoles
    {
        //public const ulong GLR = 313399864920899585;
        public const ulong CountryManager = 360597041883119617;
        public const ulong StateManager = 360095333029642267;
        public const ulong LargeAreaManager = 360619332323115009;
        public const ulong AreaManager = 360619303659241492;
        public const ulong Mentor = 360597276588113921;
        public const ulong Level6 = 360597388236292106;
        public const ulong Level5 = 360597449426993153;
        public const ulong Level4 = 360597582009073664;
        public const ulong Level3 = 360597612988203009;
        public const ulong Level2 = 360597652754268181;
        public const ulong Level1 = 360597684350091264;
        /*public const ulong Fireside = 299595056309075999;
        public const ulong Illinois = 299566236487122944;
        public const ulong Indiana = 299566253532643341;
        public const ulong Michigan = 299566187791253504;
        public const ulong Ohio = 299566271253577740;
        public const ulong Wisconsin = 299566287158509568;*/
    }
}