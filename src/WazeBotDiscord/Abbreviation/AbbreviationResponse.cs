﻿using System;
using System.Collections.Generic;
using System.Text;
using Discord;

namespace WazeBotDiscord.Abbreviation
{
    public class AbbreviationResponse
    {
        public string message { get; set; }
        public Embed results { get; set; }
    }
}
