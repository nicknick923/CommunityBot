﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WazeBotDiscord.Profile
{
    class ProfileResult
    {
        public string EditorProfile { get; set; }

        public string ForumProfile { get; set; }

        public string WikiProfile { get; set; }

        public string EditorName { get; set; }
    }
}
